FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code/web /app/code/sfu /app/code/c2c /app/code/p2p /app/code/bro /app/pkg

# peer to peer backend
WORKDIR /app/code/p2p
RUN curl -L https://github.com/miroslavpejic85/mirotalk/archive/refs/heads/master.tar.gz | tar -xz --strip-components 1 -f - -C .
RUN npm i

# broadcast backend
WORKDIR /app/code/bro
RUN curl -L https://github.com/miroslavpejic85/mirotalkbro/archive/refs/heads/master.tar.gz | tar -xz --strip-components 1 -f - -C .
RUN npm i

# selective forwarding unit backend
WORKDIR /app/code/sfu
RUN curl -L https://github.com/miroslavpejic85/mirotalksfu/archive/refs/heads/master.tar.gz | tar -xz --strip-components 1 -f - -C .
RUN npm i
RUN mv /app/code/sfu/app/src/config.template.js /app/code/sfu/app/src/config.js

# client to client backend
WORKDIR /app/code/c2c
RUN curl -L https://github.com/miroslavpejic85/mirotalkc2c/archive/refs/heads/master.tar.gz | tar -xz --strip-components 1 -f - -C .
RUN npm i

# web ui
WORKDIR /app/code/web
RUN curl -L https://github.com/miroslavpejic85/mirotalkwebrtc/archive/refs/heads/master.tar.gz | tar -xz --strip-components 1 -f - -C .
RUN npm i
RUN mv /app/code/web/backend/config.template.js /app/pkg/web.config.template.js && ln -s /run/web.config.js /app/code/web/backend/config.js && \
    mv /app/code/web/.env.template /app/pkg/web.env.template && ln -s /run/web.env /app/code/web/.env

# configure supervisor
RUN rm -rf /etc/supervisor/conf.d && ln -s /run/supervisor.conf.d /etc/supervisor/conf.d
RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf
COPY supervisor /app/pkg/supervisor

COPY start.sh patchConfig.js /app/pkg/

WORKDIR /app/pkg
CMD [ "/app/pkg/start.sh" ]
