#!/bin/bash

set -eu

echo "=> Generate web env"
if [[ ! -f /app/data/web.env ]]; then
    touch /app/data/web.env
    echo -e "LOG_COLOR=true" >> /app/data/web.env
    echo -e "LOG_DEBUG=true" >> /app/data/web.env
    # TODO make this unique per instance
    echo -e "JWT_KEY=3c125de1-e71b-4503-927b-0fb4f7d3e482" >> /app/data/web.env
fi

cp /app/pkg/web.env.template /run/web.env
cat /app/data/web.env >> /run/web.env

echo -e "SERVER_HOST=${CLOUDRON_APP_DOMAIN}" >> /run/web.env
echo -e "SERVER_PORT=8000" >> /run/web.env
echo -e "SERVER_URL=https://${CLOUDRON_APP_DOMAIN}" >> /run/web.env
echo -e "USER_REGISTRATION_MODE=true" >> /run/web.env
echo -e "JWT_EXP=24h" >> /run/web.env
echo -e "MAIL_VERIFICATION=true" >> /run/web.env
echo -e "EMAIL_HOST=${CLOUDRON_MAIL_SMTP_SERVER}" >> /run/web.env
echo -e "EMAIL_PORT=${CLOUDRON_MAIL_SMTP_PORT}" >> /run/web.env
echo -e "EMAIL_USERNAME=${CLOUDRON_MAIL_SMTP_USERNAME}" >> /run/web.env
echo -e "EMAIL_PASSWORD=${CLOUDRON_MAIL_SMTP_PASSWORD}" >> /run/web.env
echo -e "MONGO_URL=${CLOUDRON_MONGODB_URL}" >> /run/web.env
echo -e "MONGO_DATABASE=${CLOUDRON_MONGODB_DATABASE}" >> /run/web.env

echo "=> Generate config.js"
node /app/pkg/patchConfig.js

echo "=> Enable available backends"
mkdir -p /run/supervisor.conf.d
cp /app/pkg/supervisor/web.conf /run/supervisor.conf.d/
[[ ! -z "${BRO_SERVER_DOMAIN}" ]] && cp /app/pkg/supervisor/bro.conf /run/supervisor.conf.d/
[[ ! -z "${SFU_SERVER_DOMAIN}" ]] && cp /app/pkg/supervisor/sfu.conf /run/supervisor.conf.d/
[[ ! -z "${P2P_SERVER_DOMAIN}" ]] && cp /app/pkg/supervisor/p2p.conf /run/supervisor.conf.d/
[[ ! -z "${C2C_SERVER_DOMAIN}" ]] && cp /app/pkg/supervisor/c2c.conf /run/supervisor.conf.d/

echo "=> Starting MiroTalk"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i MiroTalk
