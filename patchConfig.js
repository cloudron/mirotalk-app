#!/usr/bin/env node

'use strict';

// https://github.com/miroslavpejic85/mirotalkwebrtc/blob/master/backend/config.template.js

const fs = require('fs');
const config = require('/app/pkg/web.config.template.js');

let userConfig;
try {
    userConfig = require('/app/data/web.config.json');
} catch (e) {
    userConfig = {};
}

console.log('==> Upstream default web.config.js:');
console.log(config);

const broDomain = process.env['BRO_SERVER_DOMAIN'];
const c2cDomain = process.env['C2C_SERVER_DOMAIN'];
const p2pDomain = process.env['P2P_SERVER_DOMAIN'];
const sfuDomain = process.env['SFU_SERVER_DOMAIN'];

config.MiroTalk = config.MiroTalk || {};
config.MiroTalk.P2P = config.MiroTalk.P2P || {};
config.MiroTalk.P2P.GitHub = config.MiroTalk.P2P.GitHub || {};

config.MiroTalk = config.MiroTalk || {};
config.MiroTalk.SFU = config.MiroTalk.SFU || {};
config.MiroTalk.SFU.GitHub = config.MiroTalk.SFU.GitHub || {};

config.MiroTalk = config.MiroTalk || {};
config.MiroTalk.BRO = config.MiroTalk.BRO || {};
config.MiroTalk.BRO.GitHub = config.MiroTalk.BRO.GitHub || {};

config.MiroTalk = config.MiroTalk || {};
config.MiroTalk.C2C = config.MiroTalk.C2C || {};
config.MiroTalk.C2C.GitHub = config.MiroTalk.C2C.GitHub || {};

config.MiroTalk.P2P.Visible = !!p2pDomain;
config.MiroTalk.P2P.GitHub.Visible = false;
if (p2pDomain) {
    config.MiroTalk.P2P.Home = `https://${p2pDomain}`;
    config.MiroTalk.P2P.Room = `https://${p2pDomain}/newcall`;
    config.MiroTalk.P2P.Join = `https://${p2pDomain}/join/`;
}

config.MiroTalk.SFU.Visible = !!sfuDomain;
config.MiroTalk.SFU.GitHub.Visible = false;
if (sfuDomain) {
    config.MiroTalk.SFU.Home = `https://${sfuDomain}`;
    config.MiroTalk.SFU.Room = `https://${sfuDomain}/newroom`;
    config.MiroTalk.SFU.Join = `https://${sfuDomain}/join/`;
}

config.MiroTalk.BRO.Visible = !!broDomain;
config.MiroTalk.BRO.GitHub.Visible = false;
if (broDomain) {
    config.MiroTalk.BRO.Home = `https://${broDomain}`;
    config.MiroTalk.BRO.Broadcast = `https://${broDomain}/broadcast?id=`;
    config.MiroTalk.BRO.Viewer = `https://${broDomain}/viewer?id=`;
}

config.MiroTalk.C2C.Visible = !!c2cDomain;
config.MiroTalk.C2C.GitHub.Visible = false;
if (c2cDomain) {
    config.MiroTalk.C2C.Home = `https://${c2cDomain}`;
    config.MiroTalk.C2C.Room = `https://${c2cDomain}/?room=`;
}

// https://github.com/miroslavpejic85/mirotalkwebrtc/blob/master/backend/config.template.js#L58
config.BUTTONS = config.BUTTONS || {};
if (userConfig.BUTTONS) {
    [
        'setRandomRoom',
        'copyRoom',
        'shareRoom',
        'sendEmail',
        'sendSmSInvitation',
        'joinInternal',
        'joinExternal',
        'updateRow',
        'delRow',
    ].forEach(function (k) {
        if (typeof userConfig.BUTTONS[k] !== 'undefined') config.BUTTONS[k] = !!userConfig.BUTTONS[k];
    });
}

console.log('==> New web.config.js:');
console.log(config);

fs.writeFileSync('/run/web.config.js', `module.exports = ${JSON.stringify(config)}`);
